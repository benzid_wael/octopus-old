===============================
Octopus
===============================

.. image:: https://badge.fury.io/py/octopus.png
    :target: http://badge.fury.io/py/octopus

.. image:: https://travis-ci.org/benzid-wael/octopus.png?branch=master
        :target: https://travis-ci.org/benzid-wael/octopus

.. image:: https://pypip.in/d/octopus/badge.png
        :target: https://pypi.python.org/pypi/octopus


Octopus make it easy to create custom, pluggable web 2.0 apps. It's designed to mimic the MVC pattern of frameworks like Django and Ruby on Rails.

* Free software: BSD license
* Documentation: https://octopus.readthedocs.org.

Features
--------

* TODO
