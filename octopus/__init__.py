# -*- coding: utf-8 -*-

__author__ = 'Wael BEN ZID ELGUEBSI'
__email__ = 'benzid.wael@hotmail.fr'
__version__ = '0.0.0'
__description__ = ("Octopus make it easy to create custom, pluggable web 2.0"
                   " apps. It's designed to mimic the MVC pattern of"
                   " frameworks like Django and Ruby on Rails.")
