-r base.txt

flake8==2.3.0

coverage==3.7.1
tox==1.8.1

Sphinx==1.2.3

wheel==0.23.0
